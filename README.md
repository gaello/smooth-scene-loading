# Smooth scene loading in Unity

This repository contain example how you can create loading screen with asynchronous scene loading in Unity.

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2021/02/15/smooth-scene-loading/

Enjoy!

---

# How to use it?

Clone this repository to you computer or browse it online!

If you want to see that implementation, go straight to [Assets/Scripts/](Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
